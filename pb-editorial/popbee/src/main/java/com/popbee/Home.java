package com.popbee;

import android.app.Fragment;
import android.content.Intent;
import android.os.Bundle;
import android.os.PersistableBundle;
import android.support.v7.app.ActionBar;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;


import com.google.android.gms.ads.AdListener;
import com.google.android.gms.ads.AdRequest;
import com.google.android.gms.ads.InterstitialAd;
import com.hkm.advancedtoolbar.V3.LayoutAsset;
import com.hkm.advancedtoolbar.V3.TopBarManager;
import com.hkm.advancedtoolbar.V3.layout.SimpleSearchCallBack;
import com.hkm.advancedtoolbar.V5.BeastBar;
import com.hkm.advancedtoolbar.materialsearch.MaterialSearchView;
import com.hkm.slidingmenulib.Util.Utils;
import com.hkm.slidingmenulib.gestured.SlidingMenu;
import com.hkm.slidingmenulib.layoutdesigns.app.SlidingAppCompactActivity;
import com.neopixl.pixlui.components.textview.TextView;
import com.parse.ParseAnalytics;
import com.popbee.Dialog.ExitDialog;
import com.popbee.life.EBus;
import com.popbee.life.PBUtil;
import com.popbee.pages.featureList.CommonListTemplate;
import com.popbee.pages.expMenuP;

import com.popbee.pages.featureList.SearchTemplate;
import com.popbee.pages.featureList.skeleton;
import com.popbee.pages.mainPullSlide;
import com.squareup.otto.Subscribe;


/**
 * Created by hesk on 3/7/15.
 * This is what it is
 */
public class Home extends SlidingAppCompactActivity<Fragment> {
    private mainPullSlide sliderlayout;
    private BeastBar actionToolBar;
    private MaterialSearchView searchView;

    /**
     * Same as {@link #onCreate(Bundle)} but called for those activities created with
     * the attribute {@link android.R.attr#persistableMode} set to
     * <code>persistAcrossReboots</code>.
     *
     * @param savedInstanceState if the activity is being re-initialized after
     *                           previously being shut down then this Bundle contains the data it most
     *                           recently supplied in {@link #onSaveInstanceState}.
     *                           <b><i>Note: Otherwise it is null.</i></b>
     * @param persistentState    if the activity is being re-initialized after
     *                           previously being shut down or powered off then this Bundle contains the data it most
     *                           recently supplied to outPersistentState in {@link #onSaveInstanceState}.
     *                           <b><i>Note: Otherwise it is null.</i></b>
     * @see #onCreate(Bundle)
     * @see #onStart
     * @see #onSaveInstanceState
     * @see #onRestoreInstanceState
     * @see #onPostCreate
     */
    @Override
    public void onCreate(Bundle savedInstanceState, PersistableBundle persistentState) {
        super.onCreate(savedInstanceState, persistentState);
        ParseAnalytics.trackAppOpenedInBackground(getIntent());
    }

    /**
     * @return when @link{getDefaultMainActivityLayoutId} is using user specified layout and such layout contains custom action bar or custom action tool bar then this function must return TRUE to enable the configuration of the tool bar
     */
    @Override
    protected boolean forceConfigureToolBar() {
        return true;
    }

    @Override
    protected void configToolBar(final Toolbar tb) {
        searchView = (MaterialSearchView) findViewById(R.id.material_search_view);
        actionToolBar = BeastBar.withToolbar(this, tb, PBUtil.getToolBarBuilder());
        searchView.setVoiceSearch(true);
        searchView.setCursorDrawable(R.drawable.color_cursor_white);
        // searchView.setSuggestions(getSuggestions());
        searchView.setOnQueryTextListener(new MaterialSearchView.OnQueryTextListener() {
            @Override
            public boolean onQueryTextSubmit(String query) {
                //Snackbar.make(findViewById(R.id.container), "Query: " + query, Snackbar.LENGTH_LONG).show();
                // searchSubmission(query);
                if (currentFragmentNow instanceof SearchTemplate) {
                    SearchTemplate k = (SearchTemplate) currentFragmentNow;
                    k.triggerSearch(query);
                } else {
                    setinternalChangeNoToggle(SearchTemplate.B(skeleton.conSearch(query)), "search");
                }
                actionToolBar.setActionTitle(query);
                return false;
            }

            @Override
            public boolean onQueryTextChange(String newText) {
                //Do some magic
                return false;
            }
        });
        actionToolBar.setFindIconFunc(new Runnable() {
            @Override
            public void run() {
                searchView.showSearch();
            }
        });
        actionToolBar.setBackIconFunc(new Runnable() {
            @Override
            public void run() {
                toggle();
            }
        });
    }


    @Override
    protected expMenuP getFirstMenuFragment() {
        return expMenuP.newInstanceFromMenu();
    }

    @Override
    protected mainPullSlide getInitFragment() {
        return sliderlayout = new mainPullSlide();
    }

    @Override
    protected void customizeSlideMenuEdge(SlidingMenu sm) {
        sm.setMode(SlidingMenu.LEFT);
        sm.setTouchModeAbove(SlidingMenu.TOUCHMODE_MARGIN);
        sm.setBehindScrollScale(0.5f);
        sm.setFadeEnabled(true);
        sm.setFadeDegree(0.34f);
        sm.setBehindWidth(getResources().getDimensionPixelSize(R.dimen.menu_width));
        sm.requestLayout();
        sm.invalidate();
        //initMainContentFragment
    }


    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }


    /**
     * the call back listener related to the slide view
     *
     * @param vb bool type call backs
     */
    @Subscribe
    public void onEvent(EBus.Scrolling vb) {
        if (sliderlayout != null) {
            boolean first = vb.getEnabled();
            boolean v = vb.getDirectionUpFreeHand();
            if (v && first) {
                sliderlayout.triggerPullDown();
            }
            sliderlayout.setDragLayoutTouchMode(first);
        }
    }

  /*  @Subscribe
    public void eventrefresh(EventBus.refresh vb) {
        if (vb.isEventReturnSuccess()) {
            sliderlayout.requestRefreshComplete();
        }
    }*/

    /**
     * menu control related event boardcast
     *
     * @param emenu event board cast from menu
     */
    @Subscribe
    public void onEvent(EBus.menuclass emenu) {
        if (emenu.getslug().equalsIgnoreCase("home")) {
            actionToolBar.showMainLogo();
            setinternalChange(sliderlayout = new mainPullSlide(), emenu.gettitle());
            // setinternalChange(layoutdm = new mainOriginal(), emenu.gettitle());
        } else if (emenu.getslug().equalsIgnoreCase("email")) {
            PBUtil.email(this);
            getSlidingMenu().toggle(true);
        } else {
            actionToolBar.setActionTitle(emenu.gettitle());
            setinternalChange(CommonListTemplate.B(CommonListTemplate.con_cate(emenu.getslug())), emenu.gettitle());
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        EBus.getInstance().unregister(this);
    }


    @Override
    protected void onResume() {
        super.onResume();
        EBus.getInstance().register(this);
    }

    /**
     * to produce the menu by layout inflation
     *
     * @return int with resource id
     @Override protected int getRmenu() {
     return R.menu.menu_main;
     }*/

    /**
     * starting the boardcast events in here
     */

    @Override
    protected int getDefaultMainActivityLayoutId() {
        return R.layout.main_framer;
    }

    @Override
    public void onBackPressed() {
        if (searchView.isSearchOpen()) {
            searchView.closeSearch();
        } else {
            if (!ExitDialog.Tap(getFragmentManager(), new Runnable() {
                        @Override
                        public void run() {
                            finish();
                        }
                    }
            )) {
                super.onBackPressed();
            }
        }
    }

    protected void start_interstitial() {
        final InterstitialAd interstitial = new InterstitialAd(this);
        interstitial.setAdUnitId(BuildConfig.DFP_INTER_ID);
        AdRequest adRequest = new AdRequest.Builder().build();
        interstitial.loadAd(adRequest);
        interstitial.setAdListener(new AdListener() {
            public void onAdLoaded() {
                if (interstitial.isLoaded()) {
                    interstitial.show();
                }
            }
        });
    }


    /**
     * Dispatch incoming result to the correct fragment.
     *
     * @param requestCode request
     * @param resultCode  result
     * @param data        bundle data
     */
    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        if (requestCode == Utils.RESULTSINGLE) {
            start_interstitial();
        }
    }
}