package com.popbee.life;


import com.hypebeast.sdk.api.exception.ApiException;
import com.hypebeast.sdk.api.model.popbees.mobileconfig;
import com.hypebeast.sdk.api.model.popbees.pbpost;

import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.List;

import me.drakeet.library.CrashWoodpecker;
import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;


/**
 * Created by hesk on 2/2/15.
 */
public class LifeCycleApp extends AppUtil {

    private mobileconfig mMobileConfig;
    private boolean mobileconfigready = false;

    @Override
    public void onCreate() {
        super.onCreate();
        CrashWoodpecker.fly().to(this);
        pic = Picasso.with(this);
        // Save the current Installation to Parse.
        initParseDotCom();
        //initFacebookSDK();
        googleAnalytics();
        initPB();
        initMobileConfiguration();
    }

    private void initMobileConfiguration() {
        if (mMobileConfig == null) {
            mobileconfigready = false;
            try {
                popbeeInterface().mobile_config(new Callback<mobileconfig>() {
                    @Override
                    public void success(mobileconfig mobileconfig, Response response) {
                        mMobileConfig = mobileconfig;
                        mobileconfigready = true;
                        EBus.getInstance().post(mobileconfig);
                    }

                    @Override
                    public void failure(RetrofitError error) {

                    }
                });
            } catch (ApiException e) {
                e.printStackTrace();
            }
        } else {
            mobileconfigready = true;
        }
    }

    public boolean readyconfig() {
        return mobileconfigready;
    }

    public mobileconfig seeconfg() {
        return mMobileConfig;
    }


    private static List<pbpost> container_main = new ArrayList<pbpost>();

    public List<pbpost> getMainList() {
        if (container_main == null) container_main = new ArrayList<pbpost>();
        return container_main;
    }
}
