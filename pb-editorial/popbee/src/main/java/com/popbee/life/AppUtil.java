package com.popbee.life;

import android.app.Application;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;
import android.util.Log;

//import com.facebook.FacebookSdk;
import com.facebook.ads.AdSettings;
import com.google.android.gms.analytics.GoogleAnalytics;
import com.google.android.gms.analytics.Tracker;
import com.hypebeast.sdk.api.resources.hbstore.Products;
import com.hypebeast.sdk.api.resources.pb.pbPost;
import com.hypebeast.sdk.clients.PBEditorialClient;
import com.parse.ConfigCallback;
import com.parse.Parse;
import com.parse.ParseACL;
import com.parse.ParseConfig;
import com.parse.ParseException;
import com.parse.ParseInstallation;
import com.parse.ParsePush;
import com.parse.ParseUser;
import com.parse.PushService;
import com.parse.SaveCallback;
import com.popbee.BuildConfig;
import com.popbee.Home;
import com.squareup.picasso.Picasso;

import java.net.CookieManager;

import io.realm.Realm;

/**
 * Created by hesk on 3/7/15.
 */
public abstract class AppUtil extends Application {

    public static GoogleAnalytics analytics;
    public static Tracker tracker;
    protected Picasso pic;
    protected PBEditorialClient clientPB;
    private pbPost postrequest;

    protected void initParseDotCom() {
        //CookieHandler.setDefault(cookieManager);
        // Enable Crash Reporting
        AdSettings.addTestDevice("550625490c3469958f56949d0e680195");
        // ENABLE PARSE IN HERE
        // Parse.enableLocalDatastore(this);
        // Enable and initialize the parse application
        Parse.initialize(this, BuildConfig.PARSE_APPLICATION_ID, BuildConfig.PARSE_CLIENT_KEY);
        ParsePush.subscribeInBackground("", new SaveCallback() {
            @Override
            public void done(ParseException e) {
                if (e == null) {
                    Log.d("com.parse.push", "successfully subscribed to the broadcast channel.");
                } else {
                    Log.e("com.parse.push", "failed to subscribe for push", e);
                }
            }
        });
        ParseUser.enableAutomaticUser();

        ParseACL defaultacl = new ParseACL();
        //Security of data
        // If you would like objects to be private by default, remove this line.
        defaultacl.setPublicReadAccess(true);
        ParseACL.setDefaultACL(defaultacl, true);
        ParseInstallation pi = ParseInstallation.getCurrentInstallation();
        //  PushService.subscribe(this, "ch1", RespondToPushActivity.class);
        pi.saveEventually();
    }


    public pbPost popbeeInterface() {
        return postrequest;
    }

    public Tracker getTracker() {
        return tracker;
    }

    protected void initPB() {
        clientPB = new PBEditorialClient();
        postrequest = clientPB.createPostsFeed();
    }

    protected void googleAnalytics() {
        analytics = GoogleAnalytics.getInstance(this);
        analytics.setLocalDispatchPeriod(1800);
        tracker = analytics.newTracker(BuildConfig.GOOGLE_TRACKER); // Replace with actual tracker/property Id
        tracker.enableExceptionReporting(true);
        tracker.enableAdvertisingIdCollection(false);
        tracker.enableAutoActivityTracking(true);
    }

    protected void initFacebookSDK() {
     //   FacebookSdk.sdkInitialize(getApplicationContext());
    }

    @Override
    public void onLowMemory() {
        super.onLowMemory();
    }

    @Override
    public void onTerminate() {
        super.onTerminate();
    }


}
