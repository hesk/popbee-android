package com.popbee.life;

/**
 * Android App settings
 * Created by hesk on 2/2/15.
 */
public class Config {
    public final static int NOTIFICATION_ID = 100;
    public final static boolean enableSampleData = false;
    public final static int posts_per_page = 10;
}
