package com.popbee.pages;

import android.graphics.Paint;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.widget.RelativeLayout;

import com.hkm.slidingmenulib.advancedtreeview.SmartItem;
import com.hkm.slidingmenulib.layoutdesigns.fragment.treelist;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.popbee.R;
import com.popbee.life.EBus;
import com.popbee.menu.mainMenuControlAdp;

import java.util.List;

/**
 * Created by hesk on 8/7/15.
 */
public class expMenuP extends treelist<mainMenuControlAdp, SmartItem> implements View.OnClickListener {

    public static Bundle from(final int e) {
        final Bundle n = new Bundle();
        n.putInt(REQUEST_TYPE, e);
        return n;
    }

    public static expMenuP newInstanceFromMenu() {
        final expMenuP g = new expMenuP();
        g.setArguments(from(LOADMENU));
        return g;
    }

    /**
     * this binding will be excluding the view id of
     * 1)ultimate_recycler_view
     *
     * @param before relative layout
     * @param after  relative layout
     */
    @Override
    protected void expansionBinding(RelativeLayout before, RelativeLayout after) {
        final View u = LayoutInflater.from(getActivity()).inflate(R.layout.header_layout_menu, null, false);
        //TintImageView im = (TintImageView) u.findViewById(R.id.home);
        before.addView(u);
       // im.setOnClickListener(this);
        setBackground(getActivity(), R.color.common_background);
    }

    /**
     * this is better to override it
     *
     * @return the Paint
     */
    @Override
    protected Paint getlinestyle() {
        return getsolid(R.color.divider);
    }

    /**
     * Called when a view has been clicked.
     *
     * @param v The view that was clicked.
     */
    @Override
    public void onClick(View v) {
        String[] e = new String[]{"P", "E", "F"};
        EBus.getInstance().post(new EBus.menuclass(e, "home"));
    }

    public static expMenuP newInstanceFromSystem() {
        final expMenuP g = new expMenuP();
        g.setArguments(from(LOADSYSTEM));
        return g;
    }

    @Override
    protected List<SmartItem> loadCustomMenu() {
        return mainMenuControlAdp.getPreCodeMenu(getActivity());
    }

    /**
     * load the adapter with the data list
     *
     * @return the adapter with type
     */
    @Override
    protected mainMenuControlAdp getAdatperWithdata() {
        return new mainMenuControlAdp(getActivity());
    }

    /**
     * set up the extra configurations on the ultimate recycler view
     *
     * @param listview the list with view
     * @param madapter the adapter
     */
    @Override
    protected void setUltimateRecyclerViewExtra(UltimateRecyclerView listview, mainMenuControlAdp madapter) {
    }


}
