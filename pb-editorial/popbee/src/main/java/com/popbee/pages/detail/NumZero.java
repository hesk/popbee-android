package com.popbee.pages.detail;

import android.content.Context;

import com.hkm.slider.Indicators.NumContainer;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.R;

/**
 * Created by hesk on 19/8/15.
 */
public class NumZero extends NumContainer<TextView> {
    public NumZero(Context c) {
        super(c, R.layout.slider_display);
    }
}
