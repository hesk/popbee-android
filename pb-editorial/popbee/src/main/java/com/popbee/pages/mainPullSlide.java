package com.popbee.pages;

import android.annotation.SuppressLint;
import android.app.Activity;
import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.Nullable;
import android.support.v4.view.animation.LinearOutSlowInInterpolator;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.RelativeLayout;

import com.hkm.slider.SliderLayout;
import com.hkm.slider.SliderTypes.AdvancedTextSliderView;
import com.hkm.slider.SliderTypes.BaseSliderView;
import com.hkm.slider.SliderTypes.DefaultSliderView;
import com.hkm.slider.TransformerL;
import com.hypebeast.sdk.api.model.popbees.mobileconfig;
import com.hypebeast.sdk.api.model.wprest.sliderItem;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.R;
import com.popbee.life.EBus;
import com.popbee.life.LifeCycleApp;
import com.popbee.life.PBUtil;
import com.popbee.patch.pathInterLayout;
import com.squareup.otto.Subscribe;

import java.util.Iterator;
import java.util.List;

import github.chenupt.dragtoplayout.DragTopLayout;

/**
 * Created by hesk on 13/7/15.
 */
public class mainPullSlide extends mainOriginal implements BaseSliderView.OnSliderClickListener {
    private DragTopLayout dtl;
    private SliderLayout mslider;
    private LifeCycleApp app;
    private RelativeLayout holder;
    private Handler delay = new Handler();

    protected void setup_gallery(final SliderLayout mk, final List<sliderItem> list) {
        try {
            Iterator<sliderItem> itr = list.iterator();
            while (itr.hasNext()) {
                final sliderItem itemLise = itr.next();
                DefaultSliderView textSliderView = new DefaultSliderView(getActivity());
                textSliderView
                        .description(itemLise.image)
                        .setUri(makeUri(itemLise))
                        .image(itemLise.image)
                        .setScaleType(BaseSliderView.ScaleType.CenterCrop)
                        .setOnSliderClickListener(this);
                mk.addSlider(textSliderView);
            }

            if (holder == null) {
                delay.postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        PBUtil.startToReveal(holder, 90);
                    }
                }, 100);
            } else {
                PBUtil.startToReveal(holder, 90);
            }
        } catch (NullPointerException e) {
            Log.d(TAG, e.getMessage());
        } catch (Exception e) {
            Log.d(TAG, e.getMessage());
        }
    }

    /**
     * Called when a fragment is first attached to its activity.
     * {@link #onCreate(Bundle)} will be called after this.
     *
     * @param activity
     */
    @Override
    public void onAttach(Activity activity) {
        super.onAttach(activity);
        app = (LifeCycleApp) getActivity().getApplication();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(R.layout.home_design_advanced, container, false);
    }

    @Override
    public void onViewCreated(View v, Bundle b) {
        super.onViewCreated(v, b);
        dtl = (DragTopLayout) v.findViewById(R.id.drag_layout);
        mslider = (SliderLayout) v.findViewById(R.id.layout_main_slider);
        holder = (RelativeLayout) v.findViewById(R.id.slider_holder_rl);
        pathFix = (pathInterLayout) v.findViewById(R.id.screen_content);
        dtl.setRefreshRatio(1.4f);
        dtl.listener(new DragTopLayout.SimplePanelListener() {
            @Override
            public void onRefresh() {
               /*   dtl.setRefreshing(true);
                EventBus.getInstance().post(new EventBus.refresh(EventBus.EVENT_REQUEST));
                dtl.setTop(200);*/
            }
        });
        if (mslider != null) {
            loadSlider(mslider);
        }
        // pathFix.setTouchableEat(true);
    }

    public void requestRefreshComplete() {
        dtl.onRefreshComplete();
        dtl.setTop(0);
    }

    public void setDragLayoutTouchMode(boolean b) {
        dtl.setTouchMode(b);
        //dtl.setOverDrag(b);
    }

    public void triggerPullDown() {
        if (dtl.getState() == DragTopLayout.PanelState.COLLAPSED) {
            dtl.toggleTopView();
        }
    }

    @SuppressLint("ResourceAsColor")
    protected void loadSlider(final SliderLayout vslid) {
        vslid.setOffscreenPageLimit(1);
        vslid.setSliderTransformDuration(500, new LinearOutSlowInInterpolator());
        vslid.setPresetTransformer(TransformerL.Default);
        vslid.setPresetIndicator(SliderLayout.PresetIndicators.Center_Bottom);
        vslid.getPagerIndicator().setDefaultIndicatorColor(R.color.indicator_0, R.color.indicator_1);
        /*  NumD n = new NumD(getActivity());
        n.setAlignment(NumContainer.Alignment.Center_Bottom);
        vslid.setNumLayout(n);*/
        if (app.readyconfig())
            setup_gallery(vslid, app.seeconfg().featurebanner);
    }


    @Override
    public void onSliderClick(BaseSliderView baseSliderView) {
        PBUtil.routeUri(baseSliderView.getTouchURI(), getActivity());
    }

    private Uri makeUri(final sliderItem _i) {
        if (_i.post_pid != 0) {
            Uri.Builder h = new Uri.Builder();
            return h.scheme("app").authority("popbee").appendPath(_i.post_pid + "").build();
        } else {
            return Uri.parse(_i.url);
        }
    }

    @Override
    protected void pagerChanged(int position) {

    }


    private class SmoothFadeSlidLayout extends AdvancedTextSliderView<TextView, ImageView> {

        public SmoothFadeSlidLayout(Context context) {
            super(context);
        }

        @Override
        protected int renderedLayoutTextBanner() {
            return R.layout.feature_banner_slide;
        }
    }

    /**
     * the temp sub
     * static class Slidbanner {
     * String getText() {
     * return "";
     * }
     * String getHref() {
     * return "";
     * }
     * String getImage() {
     * return "";
     * }
     * }
     */


    @Subscribe
    public void returnConfig(mobileconfig mconfig) {
        setup_gallery(mslider, mconfig.featurebanner);
    }

    @Override
    public void onStop() {
        EBus.getInstance().unregister(this);
        super.onStop();
    }

    @Override
    public void onStart() {
        EBus.getInstance().register(this);
        super.onStart();
    }
}
