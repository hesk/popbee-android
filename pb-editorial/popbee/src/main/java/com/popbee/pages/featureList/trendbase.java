package com.popbee.pages.featureList;

import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.quickAdapter.easyRegularAdapter;
import com.marshalchen.ultimaterecyclerview.quickAdapter.simpleAdmobAdapter;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.R;
import com.popbee.life.PBUtil;

import java.util.List;

import retrofit.Callback;
import retrofit.RetrofitError;

/**
 * Created by hesk on 3/7/15.
 */
public abstract class trendbase<T> extends skeleton implements Callback<List<T>> {



    @Override
    protected void onClickItem(final long post_id) {
        //Log.d(TAG, post_id + " now");
        PBUtil.routeSinglePage(post_id, getActivity());
    }

    @Override
    protected void onClickItem(final String route) {
        //Log.d(TAG, route + " now");
        PBUtil.routeSinglePage(route, getActivity());
    }


    /**
     * Unsuccessful HTTP response due to network failure, non-2XX status code, or unexpected
     * exception.
     *
     * @param error the error object
     */
    @Override
    public void failure(RetrofitError error) {
        Log.d(TAG, error.getMessage());
    }


    public static class binder extends UltimateRecyclerviewViewHolder {
        public final ImageView big_image_single;
        public final TextView tvtitle, tvtime, cate_name, rank;
        public final LinearLayout click_detection;

        public binder(View itemView) {
            super(itemView);
            big_image_single = (ImageView) itemView.findViewById(R.id.big_image_single);
            tvtitle = (TextView) itemView.findViewById(R.id.articletopic);
            tvtime = (TextView) itemView.findViewById(R.id.time_line);
            cate_name = (TextView) itemView.findViewById(R.id.cate_name);
            click_detection = (LinearLayout) itemView.findViewById(R.id.click_detection);
            rank = (TextView) itemView.findViewById(R.id.noranking);
        }
    }

    public class admob extends simpleAdmobAdapter<T, trendbase.binder, RelativeLayout> {

        public admob(RelativeLayout adview, boolean insertOnce, int setInterval, List<T> L, AdviewListener listener) {
            super(adview, insertOnce, setInterval, L, listener);
        }

        @Override
        protected void withBindHolder(trendbase.binder holder, T data, int position) {
            binddata(holder, data, position);
        }

        @Override
        protected int getNormalLayoutResId() {
            return normalLayoutResId();
        }

        @Override
        protected trendbase.binder newViewHolder(View view) {
            return new trendbase.binder(view);
        }

    }

    public class cateadapter extends easyRegularAdapter<T, binder> {

        /**
         * dynamic object to start
         *
         * @param list the list source
         */
        public cateadapter(List<T> list) {
            super(list);
        }

        /**
         * the layout id for the normal data
         *
         * @return the ID
         */
        @Override
        protected int getNormalLayoutResId() {
            return normalLayoutResId();
        }

        @Override
        protected trendbase.binder newViewHolder(View view) {
            return new trendbase.binder(view);
        }

        @Override
        protected void withBindHolder(final trendbase.binder holder, final T data, int position) {
            binddata(holder, data, position);
        }

    }

    protected abstract void binddata(final trendbase.binder holder, final T data, int position);

    protected int normalLayoutResId() {
        return R.layout.superlist_recycle_item_3;
    }

}
