package com.popbee.pages;

import android.annotation.TargetApi;
import android.app.Activity;
import android.app.Fragment;
import android.content.ComponentName;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.ActivityInfo;
import android.content.pm.PackageManager;
import android.content.pm.ResolveInfo;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;

import com.marshalchen.ultimaterecyclerview.ItemTouchListenerAdapter;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.marshalchen.ultimaterecyclerview.UltimateViewAdapter;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.life.EBus;
import com.squareup.otto.Subscribe;
import com.squareup.picasso.Picasso;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;


/**
 * Created by hesk on 29/4/15.
 */
public class sharearticles extends Fragment {

    /**
     * the event handler in life cycle
     *
     * @param b boolean in event
     */
    @Subscribe
    public void onEvent(Boolean b) {

    }

    @Override
    public void onResume() {
        super.onResume();
        EBus.getInstance().register(this);
    }

    @Override
    public void onPause() {
        super.onPause();
        EBus.getInstance().unregister(this);
    }

    public final static String
            PAGENUM = "PNUM",
            POS = "PAGEPOS",
            LOADURL = "LOAD",
            TAG = "LISTFEED";

    public static int RELOAD = 1, BYARGUMENT = -1;
    private UltimateRecyclerView pager;

    private shareactionSpline adapter;
    private LinearLayoutManager linearLayoutManager;
    private int now_page;
    private ItemTouchListenerAdapter itemInteraction;

    private List<ResolveInfo> list;

    public sharearticles() {
    }

    public interface dgg {
        public void postResults(shareactionSpline result);
    }

    public static class loadshareapps extends AsyncTask<Void, Void, shareactionSpline> {
        private Activity here;
        private String[] filterout = new String[]{
                "com.antlib.thisavhelper",
                "com.beetalk"
        };
        private dgg callback;

        public loadshareapps(Activity place, dgg cb) {
            here = place;
            callback = cb;
        }

        /**
         * Override this method to perform a computation on a background thread. The
         * specified parameters are the parameters passed to {@link #execute}
         * by the caller of this task.
         * <p/>
         * This method can call {@link #publishProgress} to publish updates
         * on the UI thread.
         *
         * @param params The parameters of the task.
         * @return A result, defined by the subclass of this task.
         * @see #onPreExecute()
         * @see #onPostExecute
         * @see #publishProgress
         */
        @Override
        protected shareactionSpline doInBackground(Void... params) {
            final PackageManager pm = here.getPackageManager();
            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "action shared");

            final List<ResolveInfo> L = pm.queryIntentActivities(shareIntent, 0);
            Iterator<ResolveInfo> itEs = L.iterator();
            while (itEs.hasNext()) {
                ResolveInfo k = itEs.next();
                //  String packageName = k.activityInfo.packageName;
                for (int j = 0; j < filterout.length; j++) {
                    if (k.activityInfo.applicationInfo.packageName.contains(filterout[j])) {
                        itEs.remove();
                    }
                }
            }
            final int applyhere = Math.min(L.size(), 10);
            return new sharearticles.shareactionSpline(L, here);
        }

        @Override
        protected void onPostExecute(shareactionSpline result) {
            callback.postResults(result);
        }

        private List<ResolveInfo> getList() /*throws IndexOutOfBoundsException */ {

            final PackageManager pm = here.getPackageManager();
            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "action shared");

            final List<ResolveInfo> L = pm.queryIntentActivities(shareIntent, 0);
            for (int i = 0; i < L.size(); i++) {
                ResolveInfo k = L.get(i);
                String packageName = k.activityInfo.packageName;
                for (int j = 0; j < filterout.length; j++) {
                    if (packageName.contains(filterout[j])) {
                        L.remove(i);
                    }
                }
            }
            final int limit_show = 20;
            final int applyhere = Math.min(L.size(), limit_show);
            if (applyhere == limit_show) {
                return L.subList(0, limit_show);
            }
            int total = L.size();
            return new ArrayList<>(L.subList(0, 20));

        }
    }

    public static shareactionSpline findapps(String[] filterout, Context here) {

        final PackageManager pm = here.getPackageManager();
        final Intent shareIntent = new Intent(Intent.ACTION_SEND);
        shareIntent.setType("text/plain");
        shareIntent.putExtra(Intent.EXTRA_TEXT, "action shared");

        final List<ResolveInfo> L = pm.queryIntentActivities(shareIntent, 0);
        Iterator<ResolveInfo> itEs = L.iterator();
        while (itEs.hasNext()) {
            ResolveInfo k = itEs.next();
            //  String packageName = k.activityInfo.packageName;
            for (int j = 0; j < filterout.length; j++) {
                if (k.activityInfo.applicationInfo.packageName.contains(filterout[j])) {
                    itEs.remove();
                }
            }
        }
           /* for (int i = 0; i < L.size(); i++) {
                ResolveInfo k = L.get(i);
                String packageName = k.activityInfo.packageName;
                for (int j = 0; j < filterout.length; j++) {
                    if (packageName.contains(filterout[j])) {
                        L.remove(i);
                    }
                }
            }*/

        final int limit_show = 20;
        final int applyhere = Math.min(L.size(), limit_show);
        if (applyhere == limit_show) {
            return new shareactionSpline(L.subList(0, limit_show), here);
        }

        return new sharearticles.shareactionSpline(L, here);
    }

    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        return inflater.inflate(com.hkm.slidingmenulib.R.layout.sm_simple_list, container, false);
    }


    private String getShareContent() {
        String title = "";
        String link = "";
        return "I just read an article about " + title + ", check it out @<a href=\"" + link + "\">read more</a>";
    }

    private void withevents(final View v) {
        try {
            pager = (UltimateRecyclerView) v.findViewById(com.hkm.slidingmenulib.R.id.ultimate_recycler_view);
            itemInteraction = new ItemTouchListenerAdapter(pager.mRecyclerView,
                    new ItemTouchListenerAdapter.RecyclerViewOnItemClickListener() {
                        @Override
                        public void onItemClick(RecyclerView parent, View clickedView, int position) {
                            Log.d(TAG, "get click view ID:" + clickedView.getId() + " pos:" + position);
                            // getActivity().startActivity(newpage(position));

                            ActivityInfo activity = list.get(position).activityInfo;
                            ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);

                            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
                            shareIntent.setType("text/plain");
                            shareIntent.putExtra(Intent.EXTRA_TEXT, getShareContent());

                            Intent newIntent = (Intent) shareIntent.clone();
                            newIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
                            newIntent.setComponent(name);
                            getActivity().startActivity(newIntent);

                        }

                        @Override
                        public void onItemLongClick(RecyclerView parent, View clickedView, int position) {
                     /*
                        if (isDrag) {
                            URLogs.d("onItemLongClick()" + isDrag);
                            toolbar.startActionMode(CustomSwipeToRefreshRefreshActivity.this);
                            toggleSelection(position);
                            dragDropTouchListener.startDrag();
                            ultimateRecyclerView.enableDefaultSwipeRefresh(false);
                        }
*/
                        }
                    });
            pager.mRecyclerView.addOnItemTouchListener(itemInteraction);
            /* View newlayout = LayoutInflater.from(getActivity()).inflate(R.layout.custom_bottom_progressbar, null);*/
            //adapter.setCustomLoadMoreView(newlayout);
            pager.setLayoutManager(new GridLayoutManager(getActivity(), 4));
            pager.setAdapter(adapter);
            //  pager.enableLoadmore();
            //reload(BYARGUMENT);
        } catch (Exception e) {
        }
    }


    private String[] filterout = new String[]{
            "com.antlib.thisavhelper",
            "com.beetalk"
    };


    @TargetApi(Build.VERSION_CODES.JELLY_BEAN_MR1)
    @Override
    public void onViewCreated(final View v, Bundle b) {

        try {
            adapter = findapps(filterout, getActivity());
            list = adapter.getListContent();
            withevents(v);
        } catch (IndexOutOfBoundsException e) {
            Log.d(TAG, e.getLocalizedMessage());
        } catch (Exception e) {
            Log.d(TAG, e.getLocalizedMessage());
        }
    }


    public static class shareactionSpline extends UltimateViewAdapter implements DialogInterface.OnClickListener {

        private List<ResolveInfo> stringList;
        final Picasso pica;
        private PackageManager pm;
        private Context mcontext;

        public shareactionSpline(List<ResolveInfo> strlist, Context ctx) {
            stringList = strlist;
            pica = Picasso.with(ctx);
            mcontext = ctx;
            pm = mcontext.getPackageManager();
        }

        public List<ResolveInfo> getListContent() {
            return stringList;
        }

        @Override
        public void onBindViewHolder(RecyclerView.ViewHolder holder, int position) {
            if (position < getItemCount() && (customHeaderView != null ? position <= stringList.size() : position < stringList.size()) && (customHeaderView != null ? position > 0 : true)) {
                try {
                    ResolveInfo d = stringList.get(customHeaderView != null ? position - 1 : position);
                    ViewHolder h = (ViewHolder) holder;
                    h.commentline.setText(d.loadLabel(pm));
                    h.imageViewSample.setImageDrawable(d.loadIcon(pm));
                    //  pica.l(d.loadIcon(pm)).into(h.imageViewSample);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
        }

        protected int getResLayoutId() {
            return com.hkm.slidingmenulib.R.layout.superlist_recycle_item_share;
        }

        @Override
        public int getAdapterItemCount() {
            return stringList.size();
        }

        @Override
        public UltimateRecyclerviewViewHolder getViewHolder(View view) {
            return new UltimateRecyclerviewViewHolder(view);
        }

        @Override
        public UltimateRecyclerviewViewHolder onCreateViewHolder(ViewGroup parent) {
            View v = LayoutInflater.from(parent.getContext()).inflate(getResLayoutId(), parent, false);
            ViewHolder vh = new ViewHolder(v);
            return vh;
        }

        @Override
        public void toggleSelection(int pos) {
            super.toggleSelection(pos);
        }

        @Override
        public void setSelected(int pos) {
            super.setSelected(pos);
        }

        @Override
        public long generateHeaderId(int i) {
            return 0;
        }

        @Override
        public void clearSelection(int pos) {
            super.clearSelection(pos);
        }


        public void swapPositions(int from, int to) {
            swapPositions(stringList, from, to);
        }

        /**
         * This method will be invoked when a button in the dialog is clicked.
         *
         * @param dialog The dialog that received the click.
         * @param which  The button that was clicked (e.g.
         *               {@link android.content.DialogInterface#BUTTON1}) or the position
         */
        @Override
        public void onClick(DialogInterface dialog, int which) {
            ActivityInfo activity = stringList.get(which).activityInfo;
            ComponentName name = new ComponentName(activity.applicationInfo.packageName, activity.name);

            final Intent shareIntent = new Intent(Intent.ACTION_SEND);
            shareIntent.setType("text/plain");
            shareIntent.putExtra(Intent.EXTRA_TEXT, "share content in here");


            // Intent newIntent = (Intent) shareIntent.clone();
            //| Intent.FLAG_ACTIVITY_RESET_TASK_IF_NEEDED
            shareIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
            shareIntent.setComponent(name);
            mcontext.startActivity(shareIntent);
        }

        @Override
        public RecyclerView.ViewHolder onCreateHeaderViewHolder(ViewGroup viewGroup) {
            return null;
        }

        @Override
        public void onBindHeaderViewHolder(RecyclerView.ViewHolder viewHolder, int i) {

        }


        class ViewHolder extends UltimateRecyclerviewViewHolder {

            private TextView commentline;
            private ImageView imageViewSample;

            public ViewHolder(View itemView) {
                super(itemView);
                commentline = (TextView) itemView.findViewById(com.hkm.slidingmenulib.R.id.textframe);
                imageViewSample = (ImageView) itemView.findViewById(com.hkm.slidingmenulib.R.id.itemframe);
            }

        }

    }

}
