package com.popbee.pages.detail;

import android.os.Bundle;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;

import com.hkm.slidingmenulib.layoutdesigns.fragment.catelog;
import com.hkm.slidingmenulib.menucontent.sectionPlate.touchItems.easyAdapter;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerView;
import com.marshalchen.ultimaterecyclerview.UltimateRecyclerviewViewHolder;
import com.neopixl.pixlui.components.textview.TextView;
import com.popbee.R;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hesk on 9/7/15.
 */
public abstract class extrapostbase<T> extends catelog<extrapostbase.ccAdp, extrapostbase.binder> {
    @Override
    protected int getUltimate_recycler_viewResId() {
        return R.id.displayList;
    }

    @Override
    protected int getFragmentResId() {
        return R.layout.relatedpost;
    }


    protected int getColumn() {
        return 2;
    }


    protected extrapostbase.ccAdp getAdatperWithdata() {
        final List<T> post = new ArrayList<>();
        return new extrapostbase.ccAdp(post);
    }


    protected void setUltimateRecyclerViewExtra(UltimateRecyclerView listview, extrapostbase.ccAdp madapter) {

    }

    /**
     * step 1:
     * takes the arguement form the intent bundle and determine if there is a need to queue a loading process. If that is a yes then we need to load up the data before displaying the list out.
     *
     * @param r and the data bundle
     * @return tells if  there is a loading process to be done before hand
     */
    @Override
    protected boolean onArguments(Bundle r) {
        return true;
    }


    public static class binder extends UltimateRecyclerviewViewHolder {
        public final ImageView im;
        public final TextView product_title, subline_left, subline_right;

        public binder(View itemView) {
            super(itemView);
            im = (ImageView) itemView.findViewById(R.id.imageholder);
            product_title = (TextView) itemView.findViewById(R.id.product_title);
            subline_left = (TextView) itemView.findViewById(R.id.subline_left);
            subline_right = (TextView) itemView.findViewById(R.id.subline_right);
        }
    }

    public class ccAdp extends easyAdapter<T, binder> {

        /**
         * dynamic object to start
         *
         * @param list the list source
         */
        public ccAdp(List<T> list) {
            super(list);
        }

        /**
         * the layout id for the normal data
         *
         * @return the ID
         */
        @Override
        protected int getNormalLayoutResId() {
            return R.layout.relatedpostitem;
        }

        @Override
        protected extrapostbase.binder newViewHolder(View view) {
            return new extrapostbase.binder(view);
        }

        @Override
        public int getAdapterItemCount() {
            return Math.min(super.getAdapterItemCount(), getDisplayItemLimit());
        }

        @Override
        protected void withBindHolder(final binder holder, final T data, int position) {
            picasso.load(getPostImageURL(data)).into(holder.im);
            holder.im.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        onClickItem(getSingleEnd(data));
                        onClickItem(getSinglePID(data));
                    } catch (Exception e) {
                        //todo: there might some error from getting the post ID
                        e.printStackTrace();
                    }
                }
            });
            holder.product_title.setText(getTextTitle(data));
            holder.subline_left.setText(getTextCate(data));
            holder.subline_right.setText(getTextTime(data));
        }

        @Override
        public UltimateRecyclerviewViewHolder getViewHolder(View view) {
            return new UltimateRecyclerviewViewHolder(view);
        }
    }

    protected abstract void onClickItem(long pid);

    /**
     * set and force the limit of the items to be able to display on the grid view
     *
     * @return the count
     */
    protected abstract int getDisplayItemLimit();

    /**
     * get data by its data type
     *
     * @param data the type of the data
     * @return the string
     */
    protected abstract String getPostImageURL(T data);

    /**
     * get data by its data type
     *
     * @param data the type of the data
     * @return the string
     */
    protected abstract String getSingleEnd(T data);

    /**
     * just to find the PID of the post
     *
     * @param data from the url
     * @return the long for PID
     */
    protected abstract long getSinglePID(T data);

    /**
     * get data by its data type
     *
     * @param data the type of the data
     * @return the string
     */
    protected abstract String getTextTitle(T data);

    /**
     * get data by its data type
     *
     * @param data the type of the data
     * @return the string
     */
    protected abstract String getTextCate(T data);

    /**
     * get data by its data type
     *
     * @param data the type of the data
     * @return the string
     */
    protected abstract String getTextTime(T data);


}
